FROM sunpeek/poetry:py3.8-slim

RUN mkdir /app
WORKDIR /app
COPY . /app
RUN poetry install

EXPOSE 8000

CMD poetry run uvicorn cryptonews_sentiment_base_model.api:app --reload

